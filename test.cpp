#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>

void *child(void *args){
	char **a=(char **)args;
	memcpy(a[2],a[1],20);
	pthread_exit(0);
}

class Foo{
	public:
		char s1[20],s2[20];
		Foo(){
			s1[0]=0;
			strcpy(s1,"Hello World!");
		}
		void run(){
			char *a[2]={s1,s2};
			pthread_t t;
			pthread_create(&t,NULL,child,a);
			pthread_join(t,NULL);
			printf("%s\n",s2);
		}
};

int main(){
	Foo f;
	f.run();
	return 0;
}